﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

using VB = Microsoft.VisualBasic.FileIO;

using System.Threading;


namespace DuplicatedFilesRemover
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            LoadDefaultSettings();
        }

        // Public VARS:
        string appName = "DuplicatedFilesRemover v.1.4";

        bool threadsEnabledToRun = false;

        int totalFileNum = 0;
        string set1Folder = null;
        string set2Folder = null;
        //int totalFileNumCompare = 0;

        //System.Collections.Generic.IEnumerable<string> set1;
        string[] set1;

        //System.Collections.Generic.IEnumerable<string> set2;
        string[] set2;
        //List<string> set2 = new List<string>();
        List<string> set1_part1 = new List<string>();
        List<string> set1_part2 = new List<string>();
        List<string> set1_part3 = new List<string>();
        List<string> set1_part4 = new List<string>();

        List<string> set1_part5 = new List<string>();
        List<string> set1_part6 = new List<string>();
        List<string> set1_part7 = new List<string>();
        List<string> set1_part8 = new List<string>();

        List<List<string>> listsPartsCollection = new List<List<string>>();
        int partsCompleted;

        //string[] set1_part1;
        //string[] set1_part2;
        //string[] set1_part3;
        //string[] set1_part4;

        Stopwatch sw = new Stopwatch();
        Stopwatch timeElapsed = new Stopwatch();
        int counterDeleted = 0;
        int counterImgsPerSec = 0;
        int counterImgsPerSecBuffer = 0;

        //public List<Thread> listThreads;
        public List<Thread> listThreads = new List<Thread>();
        //public Thread thread_1;
        //public Thread thread_2;
        //public Thread thread_3;
        //public Thread thread_4;

        public IEnumerable<List<string>> set1_divided;
        //public IEnumerable<string> set1_divided;
        //public List<string> set1_divided;

        private void btn_Start_Click(object sender, EventArgs e)
        {
            // Start Timer:
            timeElapsed.Start();

            //MainProgram();

            // Multi Threaded:

            // Pepare sets:
            PrepareSets();

            // Divide work to threads:
            DivideWorkToThreads();
        }

        private void PrepareSets()
        {
            try
            {
                // Collect all Threads in the Collection list:
                //listThreads.Add(thread_1);
                //listThreads.Add(thread_2);
                //listThreads.Add(thread_3);
                //listThreads.Add(thread_4);

                set1Folder = txtb_baseFolder.Text;
                totalFileNum = Directory.GetFiles(set1Folder, "*.*", SearchOption.AllDirectories).Length;
                //set1 = Directory.EnumerateFiles(set1Folder, "*.*", SearchOption.AllDirectories);
                set1 = Directory.GetFiles(set1Folder, "*.*", SearchOption.AllDirectories);

                // Set set2:
                if (chk_Compare.Checked)
                {
                    set2Folder = txt_CompareFolder.Text;
                    totalFileNum = Directory.GetFiles(set2Folder, "*.*", SearchOption.AllDirectories).Length;
                    //set1 = Directory.EnumerateFiles(set1Folder, "*.*", SearchOption.AllDirectories);
                    set2 = Directory.GetFiles(set2Folder, "*.*", SearchOption.AllDirectories);
                }

                // Divide the set to 4 groups:
                int numOfElements = set1.Count();

                //set1_divided = set1.Select((value, index) => new { Index = index, Value = value })
                //  //.GroupBy(x => x.Index / 5)
                //  .GroupBy(x => x.Index / (numOfElements / 4))
                //  .Select(g => g.Select(x => x.Value).ToList())
                //  .ToList();

                // Split the set1 into 4 parts:
                //for (int p = 0; p <= 3; p++)


                // 8 Threads:

                for (int i = 0; i <= set1.Count() / 8; i++)
                {
                    set1_part1.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 1) - 1; i <= (set1.Count() + 0) / 8 * 2; i++)
                {
                    set1_part2.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 2) - 1; i <= (set1.Count() + 0) / 8 * 3; i++)
                {
                    set1_part3.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 3) - 1; i <= (set1.Count() + 0) / 8 * 4; i++)
                {
                    set1_part4.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 4) - 1; i <= (set1.Count() + 0) / 8 * 5; i++)
                {
                    set1_part5.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 5) - 1; i <= (set1.Count() + 0) / 8 * 6; i++)
                {
                    set1_part6.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 6) - 1; i <= (set1.Count() + 0) / 8 * 7; i++)
                {
                    set1_part7.Add(set1[i]);
                }
                for (int i = (set1.Count() / 8 * 7) - 1; i < (set1.Count()) / 8 * 8; i++)
                {
                    set1_part8.Add(set1[i]);
                }

                // 4 Threads:

                //for (int i = 0; i <= set1.Count() / 4; i++)
                //{
                //    set1_part1.Add(set1[i]);
                //}
                //for (int i = set1.Count() / 4; i <= set1.Count() / 2; i++)
                //{
                //    set1_part2.Add(set1[i]);
                //}
                //for (int i = set1.Count() / 2; i <= set1.Count() / 4 * 3; i++)
                //{
                //    set1_part3.Add(set1[i]);
                //}
                //for (int i = set1.Count() / 4 * 3; i < set1.Count(); i++)
                //{
                //    set1_part4.Add(set1[i]);
                //}


                // Combine parts in the Collection List:               
                listsPartsCollection.Add(set1_part1);
                listsPartsCollection.Add(set1_part2);
                listsPartsCollection.Add(set1_part3);
                listsPartsCollection.Add(set1_part4);

                listsPartsCollection.Add(set1_part5);
                listsPartsCollection.Add(set1_part6);
                listsPartsCollection.Add(set1_part7);
                listsPartsCollection.Add(set1_part8);



                // Compare: set1 VS set2
                if (chk_Compare.Checked)
                {
                    set2Folder = txt_CompareFolder.Text;
                    //totalFileNumCompare = Directory.GetFiles(set2Folder, "*.*", SearchOption.AllDirectories).Length;
                    //set2 = Directory.EnumerateFiles(set2Folder, "*.*", SearchOption.AllDirectories);
                    //set2 = Directory.GetFiles(set2Folder, "*.*", SearchOption.AllDirectories);
                }
                // Compare: set1 VS set1
                else
                {
                    set2 = set1;
                }

                progressBar1.Invoke((MethodInvoker)delegate
                {
                    progressBar1.Value = 1;
                    progressBar1.Minimum = 1;
                    progressBar1.Maximum = totalFileNum;
                    progressBar1.Step = 1;
                });
                //progressBar1.Value = 1;
                //progressBar1.Minimum = 1;
                //progressBar1.Maximum = totalFileNum;
                //progressBar1.Step = 1;

                counterDeleted = 0;
                lbl_deleted.Text = "Deleted: " + counterDeleted + " Files";
            }
            catch (Exception e) { MessageBox.Show(e.Message + "\n\n" + e.StackTrace); return; }
        }

        //static IEnumerable<string> Split(string str, int chunkSize)
        //{
        //    return Enumerable.Range(0, str.Length / chunkSize)
        //        .Select(i => str.Substring(i * chunkSize, chunkSize));
        //}

        private void DivideWorkToThreads()
        {
            //for (int p = 0; p <= 3; p++)
            foreach (List<string> list in listsPartsCollection)
            {
                //Thread t = listThreads[i];
                //List<string> thisList = set1_divided.ToList(;
                //IEnumerable<string> listDivided = set1_divided[i];

                // Start Threads:
                threadsEnabledToRun = true;
                //thread_1 = new Thread(new System.Threading.ThreadStart(DuplicatedFilesRemoverTask(listsPartsCollection[i])));
                //thread_1 = new Thread(new ParameterizedThreadStart(DuplicatedFilesRemoverTask));

                //Thread newThread = new Thread(() => DuplicatedFilesRemoverTask(listsPartsCollection[p]));
                Thread newThread = new Thread(() => DuplicatedFilesRemoverTask(list));

                // Add the thread to the list:
                listThreads.Add(newThread);

                //DuplicatedFilesRemoverTask(listDivided);
                //listThreads[i].Start(listDivided);
                newThread.Start();
            }
        }

        private void WaitForThreadsToFinish()
        {
            while (partsCompleted < listsPartsCollection.Count())
            {
                // Waiting for all the parts to complete ...

            }
            //foreach (Thread runningThread in listThreads)
            //{
            //    runningThread.Join();
            //}
        }

        private void DuplicatedFilesRemoverTask(List<string> set1_x)
        {
            try
            {
                // Compare every file:
                foreach (string f1 in set1_x)
                {
                    if (threadsEnabledToRun)
                    {
                        var file1 = Path.GetFileName(f1);

                        // With every file:
                        if (set2.Count() > 0)
                        {
                            foreach (string f2 in set2)
                            {
                                try
                                {
                                    var file2 = Path.GetFileName(f2);

                                    // Check for equality of FileNames:
                                    if (file1 == file2)
                                    {
                                        // Check for equality of Full FileNames:
                                        if (f1 != f2)
                                        {
                                            // Check files byte by byte:
                                            if (CheckIfFilesAreTheSame(f1, f2))
                                            {
                                                // File is located in different folders, Delete it:
                                                if (chk_DeleteFilesToRecycleBin.Checked)
                                                {
                                                    VB.FileSystem.DeleteFile(f2, VB.UIOption.OnlyErrorDialogs, VB.RecycleOption.SendToRecycleBin);
                                                }
                                                else
                                                {
                                                    File.Delete(f2);
                                                }

                                                counterDeleted++;
                                                //lbl_deleted.Text = "Deleted: " + counterDeleted + " Files";
                                                lbl_deleted.Invoke((MethodInvoker)delegate
                                                {
                                                    lbl_deleted.Text = "Deleted: " + counterDeleted + " Files";
                                                });

                                                // Reset the set to update the last size:
                                                //set2 = Directory.EnumerateFiles(set2Folder, "*.*", SearchOption.AllDirectories);
                                                set2 = Directory.GetFiles(set2Folder, "*.*", SearchOption.AllDirectories);
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                { MessageBox.Show(e.Message + "\n\n" + e.StackTrace); }

                                counterImgsPerSecBuffer++;
                            }
                        }

                        // Folder set2 is Empty. Task Complete!
                        else
                        {
                            break;
                        }

                        // Count calculated image:
                        counterImgsPerSec++;

                        CalculateImgPerSec();

                        // Show on Progress bar:
                        progressBar1.Invoke((MethodInvoker)delegate
                        {
                            progressBar1.PerformStep();
                            lbl_progress.Text = "File: " + progressBar1.Value + "/" + totalFileNum;
                            this.Refresh();
                        });
                        //progressBar1.PerformStep();                   
                        //lbl_progress.Text = "File: " + progressBar1.Value + "/" + totalFileNum;
                        //this.Refresh();
                    }

                    // Thread wants to be closed and it is safe to do it:
                    else
                    {
                        CloseAllThreads();
                    }
                }
            }
            catch (Exception e)
            { MessageBox.Show(e.Message + "\n\n" + e.StackTrace); return; }

            // Finished:
            //  MessageBox.Show("Process Completed!");
            partsCompleted++;
            CloseAllThreads();
            CheckIfThisIsTheLastThread();
        }

        private void CalculateImgPerSec()
        {
            if (sw.ElapsedMilliseconds == 0)
            {
                sw.Start();
            }
            if (sw.ElapsedMilliseconds >= 1000)
            {
                sw.Stop();
                sw.Reset();

                // Actual Calculation:
                progressBar1.Invoke((MethodInvoker)delegate
                {
                    lbl_Speed.Text = "Speed: " + counterImgsPerSec / 1 + " Files/s";
                    lbl_Speed2.Text = "Speed (buffered): " + counterImgsPerSecBuffer / 1 + " Files/s";
                    lbl_elapsedTimer.Text = "Elapsed: " + timeElapsed.ElapsedMilliseconds / 1000 + " s";
                });
                //lbl_Speed.Text = "Speed: " + counterImgsPerSec / 1 + " Files/s";
                //lbl_Speed2.Text = "Speed (buffered): " + counterImgsPerSecBuffer / 1 + " Files/s";

                // Also calculate Estimated time:
                CalculateEstimatedTime();

                // Reset counter:
                counterImgsPerSec = 0;
                counterImgsPerSecBuffer = 0;
            }
        }

        private void CalculateEstimatedTime()
        {
            if (counterImgsPerSec > 0)
            {
                progressBar1.Invoke((MethodInvoker)delegate
                {
                    lbl_estimatedTime.Text = "Estimated: " + (totalFileNum - progressBar1.Value) / counterImgsPerSec + " s";
                });
                //lbl_estimatedTime.Text = "Estimated: " + (totalFileNum - progressBar1.Value) / counterImgsPerSec + " s";
            }
        }

        private bool CheckIfFilesAreTheSame(string f1, string f2)
        {
            byte[] fileLocal = null;
            byte[] fileRemote = null;

            try
            {
                fileLocal = File.ReadAllBytes(f1);
                fileRemote = File.ReadAllBytes(f2);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message + "\n\n" + e.StackTrace);
            }
            if (fileLocal != null)
            {
                if (fileRemote != null)
                {
                    if (fileLocal.Length == fileRemote.Length)
                    {
                        for (int i = 0; i < fileLocal.Length; i++)
                        {
                            if (fileLocal[i] != fileRemote[i])
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        private void CheckIfThisIsTheLastThread()
        {
            if (partsCompleted == listsPartsCollection.Count())
            {
                timeElapsed.Stop();
            }
        }

        private void LoadDefaultSettings()
        {
            this.Text = appName;
            txtb_baseFolder.Text = Properties.Settings.Default.set1Folder;
            txt_CompareFolder.Text = Properties.Settings.Default.set2Folder;
        }

        private void SaveDefaultSettings()
        {
            Properties.Settings.Default.set1Folder = txtb_baseFolder.Text;
            Properties.Settings.Default.set2Folder = txt_CompareFolder.Text;
            Properties.Settings.Default.Save();
        }

        private void CloseAllThreads()
        {
            //thread_1.Abort();
            //thread_1.Suspend();
            //thread_1.Interrupt();
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            //CloseAllThreads();
            threadsEnabledToRun = false;
        }

        private void chk_Compare_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Compare.Checked == true)
            {
                txt_CompareFolder.Enabled = true;
            }
            else
            {
                txt_CompareFolder.Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveDefaultSettings();
            CloseAllThreads();
        }
    }
}
