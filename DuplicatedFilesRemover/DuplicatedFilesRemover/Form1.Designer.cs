﻿namespace DuplicatedFilesRemover
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtb_baseFolder = new System.Windows.Forms.TextBox();
            this.lbl_progress = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_deleted = new System.Windows.Forms.Label();
            this.lbl_estimatedTime = new System.Windows.Forms.Label();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.lbl_Speed = new System.Windows.Forms.Label();
            this.txt_CompareFolder = new System.Windows.Forms.TextBox();
            this.chk_Compare = new System.Windows.Forms.CheckBox();
            this.lbl_Speed2 = new System.Windows.Forms.Label();
            this.grp_Folders = new System.Windows.Forms.GroupBox();
            this.grp_Options = new System.Windows.Forms.GroupBox();
            this.chk_DeleteFilesToRecycleBin = new System.Windows.Forms.CheckBox();
            this.grp_Operations = new System.Windows.Forms.GroupBox();
            this.lbl_elapsedTimer = new System.Windows.Forms.Label();
            this.grp_Folders.SuspendLayout();
            this.grp_Options.SuspendLayout();
            this.grp_Operations.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(6, 30);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(203, 77);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 233);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(922, 41);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 1;
            // 
            // txtb_baseFolder
            // 
            this.txtb_baseFolder.Location = new System.Drawing.Point(6, 55);
            this.txtb_baseFolder.Name = "txtb_baseFolder";
            this.txtb_baseFolder.Size = new System.Drawing.Size(928, 31);
            this.txtb_baseFolder.TabIndex = 2;
            this.txtb_baseFolder.Text = "C:\\Users\\Matija\\Desktop\\Tyres";
            // 
            // lbl_progress
            // 
            this.lbl_progress.AutoSize = true;
            this.lbl_progress.Location = new System.Drawing.Point(6, 202);
            this.lbl_progress.Name = "lbl_progress";
            this.lbl_progress.Size = new System.Drawing.Size(89, 25);
            this.lbl_progress.TabIndex = 3;
            this.lbl_progress.Text = "File: 0/0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Folder <set 1>:";
            // 
            // lbl_deleted
            // 
            this.lbl_deleted.AutoSize = true;
            this.lbl_deleted.Location = new System.Drawing.Point(233, 110);
            this.lbl_deleted.Name = "lbl_deleted";
            this.lbl_deleted.Size = new System.Drawing.Size(162, 25);
            this.lbl_deleted.TabIndex = 5;
            this.lbl_deleted.Text = "Deleted: 0 Files";
            // 
            // lbl_estimatedTime
            // 
            this.lbl_estimatedTime.AutoSize = true;
            this.lbl_estimatedTime.Location = new System.Drawing.Point(233, 150);
            this.lbl_estimatedTime.Name = "lbl_estimatedTime";
            this.lbl_estimatedTime.Size = new System.Drawing.Size(148, 25);
            this.lbl_estimatedTime.TabIndex = 6;
            this.lbl_estimatedTime.Text = "Estimated: 0 s";
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(6, 113);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(203, 77);
            this.btn_Stop.TabIndex = 7;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // lbl_Speed
            // 
            this.lbl_Speed.AutoSize = true;
            this.lbl_Speed.Location = new System.Drawing.Point(233, 70);
            this.lbl_Speed.Name = "lbl_Speed";
            this.lbl_Speed.Size = new System.Drawing.Size(167, 25);
            this.lbl_Speed.TabIndex = 8;
            this.lbl_Speed.Text = "Speed: 0 Files/s";
            // 
            // txt_CompareFolder
            // 
            this.txt_CompareFolder.Location = new System.Drawing.Point(6, 139);
            this.txt_CompareFolder.Name = "txt_CompareFolder";
            this.txt_CompareFolder.Size = new System.Drawing.Size(928, 31);
            this.txt_CompareFolder.TabIndex = 9;
            this.txt_CompareFolder.Text = "C:\\Users\\Matija\\Desktop\\Tyres\\tyre - googleSearch";
            // 
            // chk_Compare
            // 
            this.chk_Compare.AutoSize = true;
            this.chk_Compare.Checked = true;
            this.chk_Compare.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_Compare.Location = new System.Drawing.Point(6, 104);
            this.chk_Compare.Name = "chk_Compare";
            this.chk_Compare.Size = new System.Drawing.Size(469, 29);
            this.chk_Compare.TabIndex = 11;
            this.chk_Compare.Text = "Search folder <set 2> and Delete duplicates:";
            this.chk_Compare.UseVisualStyleBackColor = true;
            this.chk_Compare.CheckedChanged += new System.EventHandler(this.chk_Compare_CheckedChanged);
            // 
            // lbl_Speed2
            // 
            this.lbl_Speed2.AutoSize = true;
            this.lbl_Speed2.Location = new System.Drawing.Point(233, 30);
            this.lbl_Speed2.Name = "lbl_Speed2";
            this.lbl_Speed2.Size = new System.Drawing.Size(242, 25);
            this.lbl_Speed2.TabIndex = 12;
            this.lbl_Speed2.Text = "Speed (buffer): 0 Files/s";
            // 
            // grp_Folders
            // 
            this.grp_Folders.Controls.Add(this.label1);
            this.grp_Folders.Controls.Add(this.txtb_baseFolder);
            this.grp_Folders.Controls.Add(this.chk_Compare);
            this.grp_Folders.Controls.Add(this.txt_CompareFolder);
            this.grp_Folders.Location = new System.Drawing.Point(7, 12);
            this.grp_Folders.Name = "grp_Folders";
            this.grp_Folders.Size = new System.Drawing.Size(949, 182);
            this.grp_Folders.TabIndex = 13;
            this.grp_Folders.TabStop = false;
            // 
            // grp_Options
            // 
            this.grp_Options.Controls.Add(this.chk_DeleteFilesToRecycleBin);
            this.grp_Options.Location = new System.Drawing.Point(7, 200);
            this.grp_Options.Name = "grp_Options";
            this.grp_Options.Size = new System.Drawing.Size(949, 126);
            this.grp_Options.TabIndex = 14;
            this.grp_Options.TabStop = false;
            this.grp_Options.Text = "Options";
            // 
            // chk_DeleteFilesToRecycleBin
            // 
            this.chk_DeleteFilesToRecycleBin.AutoSize = true;
            this.chk_DeleteFilesToRecycleBin.Checked = true;
            this.chk_DeleteFilesToRecycleBin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_DeleteFilesToRecycleBin.Location = new System.Drawing.Point(6, 30);
            this.chk_DeleteFilesToRecycleBin.Name = "chk_DeleteFilesToRecycleBin";
            this.chk_DeleteFilesToRecycleBin.Size = new System.Drawing.Size(377, 29);
            this.chk_DeleteFilesToRecycleBin.TabIndex = 0;
            this.chk_DeleteFilesToRecycleBin.Text = "Delete files to Recycle Bin (slower)";
            this.chk_DeleteFilesToRecycleBin.UseVisualStyleBackColor = true;
            // 
            // grp_Operations
            // 
            this.grp_Operations.Controls.Add(this.lbl_elapsedTimer);
            this.grp_Operations.Controls.Add(this.btn_Start);
            this.grp_Operations.Controls.Add(this.progressBar1);
            this.grp_Operations.Controls.Add(this.lbl_progress);
            this.grp_Operations.Controls.Add(this.lbl_Speed2);
            this.grp_Operations.Controls.Add(this.lbl_deleted);
            this.grp_Operations.Controls.Add(this.lbl_Speed);
            this.grp_Operations.Controls.Add(this.lbl_estimatedTime);
            this.grp_Operations.Controls.Add(this.btn_Stop);
            this.grp_Operations.Location = new System.Drawing.Point(7, 332);
            this.grp_Operations.Name = "grp_Operations";
            this.grp_Operations.Size = new System.Drawing.Size(949, 288);
            this.grp_Operations.TabIndex = 15;
            this.grp_Operations.TabStop = false;
            this.grp_Operations.Text = "Operations";
            // 
            // lbl_elapsedTimer
            // 
            this.lbl_elapsedTimer.AutoSize = true;
            this.lbl_elapsedTimer.Location = new System.Drawing.Point(233, 190);
            this.lbl_elapsedTimer.Name = "lbl_elapsedTimer";
            this.lbl_elapsedTimer.Size = new System.Drawing.Size(131, 25);
            this.lbl_elapsedTimer.TabIndex = 13;
            this.lbl_elapsedTimer.Text = "Elapsed: 0 s";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 636);
            this.Controls.Add(this.grp_Operations);
            this.Controls.Add(this.grp_Options);
            this.Controls.Add(this.grp_Folders);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DuplicatedFilesRemover";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.grp_Folders.ResumeLayout(false);
            this.grp_Folders.PerformLayout();
            this.grp_Options.ResumeLayout(false);
            this.grp_Options.PerformLayout();
            this.grp_Operations.ResumeLayout(false);
            this.grp_Operations.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtb_baseFolder;
        private System.Windows.Forms.Label lbl_progress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_deleted;
        private System.Windows.Forms.Label lbl_estimatedTime;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Label lbl_Speed;
        private System.Windows.Forms.TextBox txt_CompareFolder;
        private System.Windows.Forms.CheckBox chk_Compare;
        private System.Windows.Forms.Label lbl_Speed2;
        private System.Windows.Forms.GroupBox grp_Folders;
        private System.Windows.Forms.GroupBox grp_Options;
        private System.Windows.Forms.CheckBox chk_DeleteFilesToRecycleBin;
        private System.Windows.Forms.GroupBox grp_Operations;
        private System.Windows.Forms.Label lbl_elapsedTimer;
    }
}

